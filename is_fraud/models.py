from django.db import models

class Sign(models.Model):
	name = models.CharField(max_length=50, primary_key=True)
	sign_1 = models.FileField(upload_to="sign_files/")
	sign_2 = models.FileField(upload_to="sign_files/")
	uploaded_at = models.DateTimeField(auto_now_add=True)