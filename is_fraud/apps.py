from django.apps import AppConfig


class IsFraudConfig(AppConfig):
    name = 'is_fraud'
