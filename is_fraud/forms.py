from django import forms
from is_fraud.models import Sign
class SignForm(forms.ModelForm):
    class Meta:
        model = Sign
        fields = ("name", "sign_1","sign_2" )