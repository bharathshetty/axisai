from __future__ import division, print_function
import io
import os 
import numpy as np

from keras import backend as K
from keras.applications import vgg16
from keras.layers import Input, merge
from keras.layers.convolutional import Conv2D, MaxPooling2D,Conv1D, MaxPooling1D
from keras.layers.core import Activation, Dense, Dropout, Flatten, Lambda
from keras.models import Sequential, Model
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from random import shuffle

from scipy.misc import imresize
import itertools
import numpy as np

from PIL import Image

from keras.preprocessing import image
from keras.models import Model, Input
from keras.layers import Dense, GlobalAveragePooling2D
from keras import backend as K

	
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage

# Create your views here.
from is_fraud.models import Sign
from is_fraud.forms import SignForm

from base64 import b64decode
import tensorflow as tf
from PIL import Image
from django.core.files.temp import NamedTemporaryFile
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

def index(request):
	signs = Sign.objects.all()
	return render(request,'is_fraud/index.html',{ 'signs': signs })

def model_form_upload(request):
    if request.method == 'POST':
        form = SignForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = SignForm()
    return render(request, 'is_fraud/model_form_upload.html', {
        'form': form
    })

 # Model code start 
img_rows = 100
img_cols = 240
input_shape=(img_rows, img_cols, 1)

def create_base_network(input_shape):
    inp = Input(shape=input_shape)
    conv_1 = Conv2D(32, kernel_size=(2, 2), activation='relu')(inp)
    conv_2 = Conv2D(64, (2, 2), activation='relu')(conv_1)
    max_p = MaxPooling2D(pool_size=(2, 2))(conv_2)
    drop = Dropout(0.25)(max_p)
    flat = Flatten()(drop)
    model = Model(inputs=inp,outputs=flat)
    return model
  
def cosine_distance(vecs, normalize=False):
    x, y = vecs
    if normalize:
        x = K.l2_normalize(x, axis=0)
        y = K.l2_normalize(x, axis=0)
    return K.prod(K.stack([x, y], axis=1), axis=1)

def cosine_distance_output_shape(shapes):
    return shapes[0]

def compute_accuracy(preds, labels):
    return labels[preds.ravel() < 0.5].mean()



base_network = create_base_network(input_shape)

image_left = Input(shape=input_shape)
image_right = Input(shape=input_shape)
vector_left = base_network(image_left)
vector_right = base_network(image_right)
distance = Lambda(cosine_distance, 
                  output_shape=cosine_distance_output_shape)([vector_left, vector_right])
fc1 = Dense(64, kernel_initializer="glorot_uniform",activation="relu")(distance)
fc1 = Dropout(0.4)(fc1)
pred = Dense(1, kernel_initializer="glorot_uniform",activation='sigmoid')(fc1)

model = Model(inputs=[image_left, image_right], outputs=pred)
model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])

model.load_weights('media/model.h5')
graph = tf.get_default_graph()
def load_image(image):
    image = image.resize(size=(240, 100))
    
    THRESHOLD_VALUE = 225
    image = image.convert("L")
    image = np.array(image)
    image = (image > THRESHOLD_VALUE) * 1.0
    return image

def check_prob(img,img2,img3):

    l = img.reshape(1,img_rows, img_cols, 1)
    r = img2.reshape(1,img_rows,img_cols, 1)
    r2 = img3.reshape(1,img_rows, img_cols, 1)
    with graph.as_default():
        cal1 = model.predict([l,r])
        cal2 = model.predict([l,r2])

    return cal1[0][0], cal2[0][0]



@csrf_exempt

def classify(request):
    data = {"success": False}

    if request.method == "POST":
        tmp_f = NamedTemporaryFile()
        tmp_f2 = NamedTemporaryFile()
        if request.FILES.get("image", None) is not None:
            image_request = request.FILES["image"]
            image_bytes = image_request.read()
            image = Image.open(io.BytesIO(image_bytes))
            image.save(tmp_f, image.format)
            name = request.POST['name'].strip('\n')
        elif request.POST.get("image64", None) is not None:
            base64_data = request.POST.get("image64", None).split(',', 1)[1]
            plain_data = b64decode(base64_data)
            image = Image.open(io.BytesIO(plain_data))
            tmp_f.write(plain_data)
            name = request.POST['name'].replace('\n','').replace(' ','')
        tmp_f.close()
        
        image = load_image(image)

        s = Sign.objects.filter(name=name).get()
        img_2_url ,img_2_url2 = s.sign_1.path,s.sign_2.path
        image2 = load_image(Image.open(img_2_url))
        image3 = load_image(Image.open(img_2_url))

        classify_result = check_prob(image,image2,image3)
        print(classify_result)
        confidence = (classify_result[0] + classify_result[1])/2
        classify_result = 'Original' if  (classify_result[0]>0.7) or (classify_result[1]>0.7) else 'Fake'

        if classify_result:
            data["success"] = True 
            data["result"] = classify_result
            data['original_sign_url'] = img_2_url
            data['confidence'] = str(confidence)
    return JsonResponse(data)
